package main;

public class Aufg3 {
			 
    static String convertFahrenheitToCelciusAsString(int fahrenheitValue)
    {
        float celsiusValue = ((fahrenheitValue - 32.0f) * 5.0f) / 9.0f;
        String celsiusValueString = String.format("%.2f", celsiusValue);

        return celsiusValueString;
    } 

    public static void main(String args[]) 
    {
        String sformat = "%1$-12s|%2$10s\n";
        String fahrenheitValueString;
        String fahrenheitValueStringOutput;
        String celsiusValueStringOutput;

        System.out.format(sformat, "Fahrenheit",  "Celsius");
        System.out.println("-----------------------");

        for(int fahrenheitValue = -20; fahrenheitValue <= 30; fahrenheitValue += 10) 
        {
            if (fahrenheitValue != 10)
            {
                celsiusValueStringOutput = convertFahrenheitToCelciusAsString(fahrenheitValue);
                fahrenheitValueString = String.valueOf(fahrenheitValue);
                //                              expression                 true                    false
                fahrenheitValueStringOutput = (fahrenheitValue < 0) ? fahrenheitValueString : "+" + fahrenheitValueString;

                System.out.format(sformat, fahrenheitValueStringOutput, celsiusValueStringOutput);
            }
        }
    }
}
